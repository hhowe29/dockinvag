#!/bin/bash -e
echo "Provisioning virtual machine..."

pm='sudo -E apt-get -qq -y'

echo "Updating package database"
$pm update

echo "Upgrading existing packages"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o DPkg::options::="--force-confdef" -o DPkg::options::="--force-confold"  upgrade grub-pc
$pm upgrade

echo "Installing python software properties"
$pm install python-software-properties

echo "Installing base utilities"
$pm install nano dos2unix tree

echo "Installing python and development libraries"
$pm install python2.7 python-dev python-pip libjpeg-dev zlib1g-dev libxml2 libxslt-dev


echo "Removing unused packages"
$pm autoremove

echo "Upgrading pip"
sudo -H pip install --upgrade pip

echo 'Installing docker-ce'
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
$pm update
$pm install docker-ce
sudo -H pip install docker-compose

if [ "${USER}" = "vagrant" ]; then
    echo "Installing python requirements"
    sudo -H pip install -r /home/vagrant/code/django/requirements.txt
fi

if [ -n "${HOST_LS_COLORS}" ]; then
  echo "Setting LS_COLORS"
  echo "export LS_COLORS=\"${HOST_LS_COLORS}\"" | tee -a /home/vagrant/.profile
else
  echo "HOST_LS_COLORS was not set"
fi

echo "Provision complete"