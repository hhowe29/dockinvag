from bar.models import InvestmentType, Investment
from datetime import date

def load_investments():
    mutual_fund_type = InvestmentType.objects.get(name='Mutual Fund')
    stock_type = InvestmentType.objects.get(name='Stock')
    money_market_type = InvestmentType.objects.get(name='Money Market')


    data = [
        ('VFINX', mutual_fund_type, 'Vanguard 500 Index Fund Investor Shares', 'Vanguard Group', date(1976, 8, 3), '922908108'),
        ('DE', stock_type, 'Deere Inc', 'TD Ameritrade', None, None),
        ('VMMXX', mutual_fund_type, 'Vanguard Prime Money Market Fund', 'Vanguard Group', date(1975, 6, 4), '922906201'),
    ]

    print 'Loading investments'
    for (symbol, type, name, institution, inception_date, cusip) in data:
        print 'Loading investment : ' + name
        Investment.objects.update_or_create(symbol=symbol, defaults=dict(
            type=type,
            name=name,
            institution=institution,
            inception_date=inception_date,
            cusip=cusip
        ))


def load_sample_data():
    print 'Loading sample data'
    load_investments()

if __name__ == "__main__":
    load_sample_data()
