from django.contrib.auth.models import User

from bar.models import InvestmentType

def create_admin_user():
    if not User.objects.filter(username='admin').exists():
        print 'Creating admin django user'
        User.objects.create_superuser(
            username='admin',
            password='29foxtrot',
            email='admin@foo.com'
        )


def load_investment_types():
    data = [
        'Mutual Fund',
        'Stock',
        'Annuity',
        'Money Market',
        'CD'
    ]

    print 'Loading investment types'
    for name in data:
        print 'Loading type : ' + name
        InvestmentType.objects.update_or_create(name=name)

def load_stock_data():
    print 'Loading stock data'
    create_admin_user()
    load_investment_types()

if __name__ == "__main__":
    load_stock_data()
