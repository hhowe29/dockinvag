from django.core.management import BaseCommand
from foo.etl.stock_data import load_stock_data

class Command(BaseCommand):
    def handle(self, *args, **options):
        load_stock_data()