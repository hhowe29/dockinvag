from django.core.management import BaseCommand
from foo.etl.sample_data import load_sample_data

class Command(BaseCommand):
    def handle(self, *args, **options):
        load_sample_data()