from django.core.management.base import BaseCommand
from django.db import connection
from time import sleep
from datetime import datetime, timedelta

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('max_wait', nargs='?', type=int, default=30)

    def handle(self, *args, **options):
        self.wait_for_database(options['max_wait'])

    def wait_for_database(self, max_wait_seconds):
        print 'Waiting up to %d seconds for the database to be ready' % max_wait_seconds

        attempt = 0
        expiration_time = datetime.utcnow() + timedelta(seconds=max_wait_seconds)
        while datetime.utcnow() < expiration_time:
            attempt += 1
            try:
                print 'Connection attempt %d' % (attempt)
                cursor = connection.cursor()
                cursor.execute('SELECT version()')
                msg = 'Successfully connected to database'
                break
            except:
                msg = 'ERROR: Database connection timed out!'
                sleep(1)

        print msg
