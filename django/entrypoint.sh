#!/bin/sh
echo Django entry point : $@

case $1 in
	migrate)
		python manage.py migrate;;
	stock_data)
		python manage.py stock_data;;
	sample_data)
		python manage.py sample_data;;
	server)
		python manage.py runserver 0.0.0.0:8080;;
	shell)
		python manage.py shell;;
	provision)
		python manage.py wait4database;
		python manage.py migrate;
		python manage.py stock_data;
		python manage.py sample_data;;
	boot)
		python manage.py wait4database;
		python manage.py migrate;
		python manage.py stock_data;
		python manage.py sample_data;

		python manage.py runserver 0.0.0.0:8080;;

	*)
		$@
esac