# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from bar.models import InvestmentType, Investment

class InvestmentTypeAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name'
    ]


class InvestmentAdmin(admin.ModelAdmin):
    list_display = [
        'symbol',
        'type',
        'name',
        'institution',
    ]

    list_filter = ['institution', 'type', 'inception_date']
    search_fields = ['symbol', 'type', 'name', 'institution']

admin.site.register(InvestmentType, InvestmentTypeAdmin)
admin.site.register(Investment, InvestmentAdmin)
