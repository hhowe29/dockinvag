# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class InvestmentType(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'investment_type'
        ordering = ['id']

    def __unicode__(self):
        return self.name

class Investment(models.Model):
    type = models.ForeignKey(to=InvestmentType)
    symbol = models.CharField(max_length=5)
    name = models.CharField(max_length=50)
    institution = models.CharField(max_length=50)
    cusip = models.CharField(max_length=9, null=True)
    inception_date = models.DateField(null=True)

    class Meta:
        db_table = 'investment'

    def __unicode__(self):
        return '%s : %s' %(self.symbol, self.name)

# class InvestmentPrice(models.Model):
#     investment = models.ForeignKey(to=Investment)
#     date = models.DateField(auto_now=True)
#     price = models.DecimalField(max_digits=14 ,decimal_places=4)
#
#     class Meta:
#         db_table = 'investment_price'
#
#     def __unicode__(self):
#         return '%s : %s : %s' %(self.investment__symbol, self.date, self.price)

