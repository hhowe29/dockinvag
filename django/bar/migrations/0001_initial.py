# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-23 15:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Investment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('symbol', models.CharField(max_length=5)),
                ('name', models.CharField(max_length=50)),
                ('institution', models.CharField(max_length=50)),
                ('cusip', models.CharField(max_length=9, null=True)),
                ('inception_date', models.DateField(null=True)),
            ],
            options={
                'db_table': 'investment',
            },
        ),
        migrations.CreateModel(
            name='InvestmentType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'investment_type',
            },
        ),
        migrations.AddField(
            model_name='investment',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bar.InvestmentType'),
        ),
    ]
